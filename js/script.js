function CalculateArea() 
{
        var txtWidth = document.getElementById("txtWidth");
        var txtLength = document.getElementById("txtLength");
        var txtArea = txtWidth.value * txtLength.value;
        var txtPerimeter = txtWidth.value * 2 + txtLength.value * 2;

        document.getElementById('txtArea').value = txtArea;
        document.getElementById('txtPerimeter').value = txtPerimeter;
}

// calculate numeric values:
txtPerimeter = document.getElementById('txtPerimeter');
txtPerimeter.value =  txtWidth * 2 + txtLength * 2; // using previously calculated txtWidth and txtLength